#!/bin/bash

docker pull simonsapathy/docker-intro:latest
docker run -d --rm --name docker-intro -p 8000:8000 simonsapathy/docker-intro:latest

sleep 10

xdg-open http://localhost:8000 &>/dev/null
